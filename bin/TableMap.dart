import 'dart:core';
import 'dart:io';
import 'Bomb.dart';
import 'CheckWin.dart';
import 'Bear.dart';
import 'Tree.dart';

class TableMap {
  num width = 0;
  num height = 0;
  late Bear bear;
  late Bomb bomb1;
  late Bomb bomb2;
  late Bomb bomb3;
  late Bomb bomb4;
  late Bomb bomb5;
  late Tree tree1;
  late Tree tree2;
  late Tree tree3;
  late Tree tree4;
  late Tree tree5;
  late Tree tree6;
  late Tree tree7;
  late Tree tree8;
  late Tree tree9;
  late Tree tree10;
  late CheckWin winner;

TableMap(num width, num height) {
    this.width = width;
    this.height = height;
  }
  void setBear(Bear bear) {
    this.bear = bear;
  }

  void setBomb(Bomb bomb1, Bomb bomb2, Bomb bomb3, Bomb bomb4, Bomb bomb5) {
    this.bomb1 = bomb1;
    this.bomb2 = bomb2;
    this.bomb3 = bomb3;
    this.bomb4 = bomb4;
    this.bomb5 = bomb5;
  }

  void setTree(Tree tree1, Tree tree2, Tree tree3, Tree tree4, Tree tree5,
      Tree tree6, Tree tree7, Tree tree8, Tree tree9, Tree tree10) {
    this.tree1 = tree1;
    this.tree2 = tree2;
    this.tree3 = tree3;
    this.tree4 = tree4;
    this.tree5 = tree5;
    this.tree6 = tree6;
    this.tree7 = tree7;
    this.tree8 = tree8;
    this.tree9 = tree9;
    this.tree10 = tree10;
  }

  void setWinner(CheckWin winner) {
    this.winner = winner;
  }
 void showMap() {
    print(bear);
    for (int y = 0; y < height; y++) {
      for (num x = 0; x < width; x++) {
        if (bear.isOn(x, y)) {
          stderr.write(bear.getSymbol());
        } else if (bomb1.isOn(x, y)) {
          stderr.write(bomb1.getSymbol());
        } else if (bomb2.isOn(x, y)) {
          stderr.write(bomb2.getSymbol());
        } else if (bomb3.isOn(x, y)) {
          stderr.write(bomb3.getSymbol());
        } else if (bomb4.isOn(x, y)) {
          stderr.write(bomb4.getSymbol());
        } else if (bomb5.isOn(x, y)) {
          stderr.write(bomb5.getSymbol());
        } else if (tree1.isOn(x, y)) {
          stderr.write(tree1.getSymbol());
        } else if (tree2.isOn(x, y)) {
          stderr.write(tree2.getSymbol());
        } else if (tree3.isOn(x, y)) {
          stderr.write(tree3.getSymbol());
        } else if (tree4.isOn(x, y)) {
          stderr.write(tree4.getSymbol());
        } else if (tree5.isOn(x, y)) {
          stderr.write(tree5.getSymbol());
        } else if (tree6.isOn(x, y)) {
          stderr.write(tree6.getSymbol());
        } else if (tree7.isOn(x, y)) {
          stderr.write(tree7.getSymbol());
        } else if (tree8.isOn(x, y)) {
          stderr.write(tree8.getSymbol());
        } else if (tree9.isOn(x, y)) {
          stderr.write(tree9.getSymbol());
        } else if (tree10.isOn(x, y)) {
          stderr.write(tree10.getSymbol());
        } else if (winner.isOn(x, y)) {
          stderr.write(winner.getSymbol());
        } else {
          stderr.write('  +   ');
        }
      }
      print('');
    }
  }

  bool inMap(num x, num y) {
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isBomb(num x, num y) {
    if (bomb1.isOn(x, y)) {
      return true;
    }
    if (bomb2.isOn(x, y)) {
      return true;
    }
    if (bomb3.isOn(x, y)) {
      return true;
    }
    if (bomb4.isOn(x, y)) {
      return true;
    }
    if (bomb5.isOn(x, y)) {
      return true;
    }
    return false;
  }

  bool isWin(num x, num y) {
    return winner.isOn(x, y);
  }

  bool isTree(num x, num y) {
    if (tree1.isOn(x, y)) {
      return true;
    }
    if (tree2.isOn(x, y)) {
      return true;
    }
    if (tree3.isOn(x, y)) {
      return true;
    }
    if (tree4.isOn(x, y)) {
      return true;
    }
    if (tree5.isOn(x, y)) {
      return true;
    }
    if (tree6.isOn(x, y)) {
      return true;
    }
    if (tree7.isOn(x, y)) {
      return true;
    }
    if (tree8.isOn(x, y)) {
      return true;
    }
    if (tree9.isOn(x, y)) {
      return true;
    }
    if (tree10.isOn(x, y)) {
      return true;
    }

    return false;
  }
}

