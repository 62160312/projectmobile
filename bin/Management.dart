abstract class Management {
  num x = 0;
  num y = 0;
  var symbol;

  num getX();
  num getY();
  String getSymbol();
  bool isOn(num x, num y);

  Management(num x, num y, String symbol) {
    this.x = x;
    this.y = y;
    this.symbol;
  }
}
