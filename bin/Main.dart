import 'dart:convert';
import 'dart:io';
import 'Bomb.dart';
import 'CheckWin.dart';
import 'Bear.dart';
import 'TableMap.dart';
import 'Tree.dart';

main(List<String> args) {
  iconGame();
  print('Game Description');
  print(
      '     You can use the w,a,s,d keys to control the robot direction of movement.');
  print(
      '     Robots have limited energy so you should think carefully with each walk.');
  print('');
  print('Symbols used in the game');
  print('   1. Bear ( ʕ•ᴥ•ʔ )');
  print('   2. Bobm  ( ☸ )');
  print('   3. Tree  ( └||┘ )');
  print('   4. winner  ( ✪ )');
  print(
      '--------------------------------------------------------------------------------');
  print('Please select the map size you want.');
  int? mapsize = int.parse(stdin.readLineSync()!);
  final map = new TableMap(mapsize, mapsize);
  //Bomb
  final bomb1 = new Bomb(5, 5, '  ☸   ');
  final bomb2 = new Bomb(3, 5, '  ☸   ');
  final bomb3 = new Bomb(4, 7, '  ☸  ');
  final bomb4 = new Bomb(8, 2, '  ☸   ');
  final bomb5 = new Bomb(6, 9, '  ☸   ');
  //Tree
  final tree1 = new Tree(7, 6, ' └||┘  ');
  final tree2 = new Tree(7, 7, ' └||┘  ');
  final tree3 = new Tree(1, 1, ' └||┘  ');
  final tree4 = new Tree(2, 9, ' └||┘  ');
  final tree5 = new Tree(9, 2, ' └||┘  ');
  final tree6 = new Tree(0, 1, ' └||┘ ');
  final tree7 = new Tree(7, 8, ' └||┘  ');
  final tree8 = new Tree(5, 8, ' └||┘ ');
  final tree9 = new Tree(4, 9, ' └||┘  ');
  final tree10 = new Tree(5, 1, '└||┘ ');
  //CheckWin
  final winner = new CheckWin(9, 9, ' ✪   ');
  //Bear
  final bear = new Bear(2, 2, 'ʕ•ᴥ•ʔ ', map, 15);
  map.setTree(
      tree1, tree2, tree3, tree4, tree5, tree6, tree7, tree8, tree9, tree10);
  map.setBomb(bomb1, bomb2, bomb3, bomb4, bomb5);
  map.setWinner(winner);
  map.setBear(bear);

  while (true) {
    map.showMap();
    String? direction = stdin.readLineSync();
    bear.walk(direction!);
    if (direction == 'q') {
      quitGame();
      stderr.writeln();
      stderr.writeln();

      break;
    }
  }
}

void quitGame() {
  stderr.writeln(
      '██████╗░██╗░░░██╗███████╗  ██████╗░██╗░░░██╗███████╗  ██████╗░██╗░░░██╗███████');
  stderr.writeln(
      '██╔══██╗╚██╗░██╔╝██╔════╝  ██╔══██╗╚██╗░██╔╝██╔════╝  ██╔══██╗╚██╗░██╔╝██╔════╝');
  stderr.writeln(
      '██████╦╝░╚████╔╝░█████╗░░  ██████╦╝░╚████╔╝░█████╗░░  ██████╦╝░╚████╔╝░█████╗░░');
  stderr.writeln(
      '██╔══██╗░░╚██╔╝░░██╔══╝░░  ██╔══██╗░░╚██╔╝░░██╔══╝░░  ██╔══██╗░░╚██╔╝░░██╔══╝░░');
  stderr.writeln(
      '██████╦╝░░░██║░░░███████╗  ██████╦╝░░░██║░░░███████╗  ██████╦╝░░░██║░░░███████');
  stderr.writeln(
      '╚═════╝░░░░╚═╝░░░╚══════╝  ╚═════╝░░░░╚═╝░░░╚══════╝  ╚═════╝░░░░╚═╝░░░╚══════╝');
}

void iconGame() {
  stderr.writeln(
      '░██╗░░░░░░░██╗███████╗██╗░░░░░░█████╗░░█████╗░███╗░░░███╗███████╗');
  stderr.writeln(
      '░██║░░██╗░░██║██╔════╝██║░░░░░██╔══██╗██╔══██╗████╗░████║██╔════╝');
  stderr.writeln(
      '░╚██╗████╗██╔╝█████╗░░██║░░░░░██║░░╚═╝██║░░██║██╔████╔██║█████╗░░');
  stderr.writeln(
      '░░████╔═████║░██╔══╝░░██║░░░░░██║░░██╗██║░░██║██║╚██╔╝██║██╔══╝░░');
  stderr.writeln(
      '░░╚██╔╝░╚██╔╝░███████╗███████╗╚█████╔╝╚█████╔╝██║░╚═╝░██║███████╗');
  stderr.writeln(
      '░░░╚═╝░░░╚═╝░░╚══════╝╚══════╝░╚════╝░░╚════╝░╚═╝░░░░░╚═╝╚══════╝');
  stderr.writeln('');
  stderr.writeln(
      '                                 ░██████╗░░█████╗░███╗░░░███╗███████╗');
  stderr.writeln(
      '                                 ██╔════╝░██╔══██╗████╗░████║██╔════╝');
  stderr.writeln(
      '                                 ██║░░██╗░███████║██╔████╔██║█████╗░░');
  stderr.writeln(
      '                                 ██║░░╚██╗██╔══██║██║╚██╔╝██║██╔══╝░░');
  stderr.writeln(
      '                                 ╚██████╔╝██║░░██║██║░╚═╝░██║███████╗');
  stderr.writeln(
      '                                 ░╚═════╝░╚═╝░░╚═╝╚═╝░░░░░╚═╝╚══════╝');
}
