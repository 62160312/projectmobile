import 'dart:io';

import 'TableMap.dart';

class Bear {
  num x = 0;
  num y = 0;
  String symbol = '';
  num fuel = 0;
  Bear(num x, num y, String symbol, TableMap map, num fuel) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
    this.map = map;
    this.fuel = fuel;
  }
  num getX() {
    return x;
  }

  num getY() {
    return y;
  }

  String getSymbol() {
    return symbol;
  }

  late TableMap map;
  bool walk(String direction) {
    switch (direction) {
      case 'w':
        if (canWalk(x, y - 1)) {
          y = y - 1;
          reduceFuel();
        } else {
          return false;
        }
        break;
      case 's':
        if (canWalk(x, y + 1)) {
          y = y + 1;
          reduceFuel();
        } else {
          return false;
        }
        break;
      case 'd':
        if (canWalk(x + 1, y)) {
          x = x + 1;
          reduceFuel();
        } else {
          return false;
        }
        break;
      case 'a':
        if (canWalk(x - 1, y)) {
          x = x - 1;
          reduceFuel();
        } else {
          return false;
        }
        break;
      default:
        return false;
    }
  if (map.isBomb(x, y)) {
      stderr.writeln('██████╗░░█████╗░███╗░░░███╗██████╗░██╗██╗██');
      stderr.writeln('██╔══██╗██╔══██╗████╗░████║██╔══██╗██║██║██');
      stderr.writeln('██████╦╝██║░░██║██╔████╔██║██████╦╝██║██║██');
      stderr.writeln('██╔══██╗██║░░██║██║╚██╔╝██║██╔══██╗╚═╝╚═╝╚═╝');
      stderr.writeln('██████╦╝╚█████╔╝██║░╚═╝░██║██████╦╝██╗██╗██╗');
      stderr.writeln('╚═════╝░░╚════╝░╚═╝░░░░░╚═╝╚═════╝░╚═╝╚═╝╚═╝');
      fuel = fuel - 5;
      return true;
    }
    if (map.isWin(x, y)) {
      
stderr.writeln('  ██████╗░███████╗░█████╗░██████╗░');
stderr.writeln('  ██╔══██╗██╔════╝██╔══██╗██╔══██╗');
stderr.writeln('  ██████╦╝█████╗░░███████║██████╔╝');
stderr.writeln('  ██╔══██╗██╔══╝░░██╔══██║██╔══██╗');
stderr.writeln('  ██████╦╝███████╗██║░░██║██║░░██║');
stderr.writeln('  ╚═════╝░╚══════╝╚═╝░░╚═╝╚═╝░░╚═╝');
      
      stderr.writeln(
          '███▄─█▀▀▀█─▄█▄─▄█▄─▀█▄─▄█▄─▀█▄─▄█▄─▄▄─█▄─▄▄▀█░█░█');
      stderr.writeln(
          '████─█─█─█─███─███─█▄▀─███─█▄▀─███─▄█▀██─▄─▄█▄█▄█');
      stderr.writeln(
          '▀▀▀▀▄▄▄▀▄▄▄▀▀▄▄▄▀▄▄▄▀▀▄▄▀▄▄▄▀▀▄▄▀▄▄▄▄▄▀▄▄▀▄▄▀▄▀▄▀');
    }
    return true;
  }

  bool canWalk(num x, num y) {
    return map.inMap(x, y) && !map.isTree(x, y) && fuel > 0;
  }

  bool isOn(num x, num y) {
    return this.x == x && this.y == y;
  }
  void reduceFuel() {
    fuel--;
    if (fuel <= 0) {
      overFuel();
    }
  }

  void overFuel() {
    
stderr.writeln('░██████╗░░█████╗░███╗░░░███╗███████╗  ░█████╗░██╗░░░██╗███████╗██████╗░');
stderr.writeln('██╔════╝░██╔══██╗████╗░████║██╔════╝  ██╔══██╗██║░░░██║██╔════╝██╔══██╗');
stderr.writeln('██║░░██╗░███████║██╔████╔██║█████╗░░  ██║░░██║╚██╗░██╔╝█████╗░░██████╔╝');
stderr.writeln('██║░░╚██╗██╔══██║██║╚██╔╝██║██╔══╝░░  ██║░░██║░╚████╔╝░██╔══╝░░██╔══██╗');
stderr.writeln('╚██████╔╝██║░░██║██║░╚═╝░██║███████╗  ╚█████╔╝░░╚██╔╝░░███████╗██║░░██║');
stderr.writeln('░╚═════╝░╚═╝░░╚═╝╚═╝░░░░░╚═╝╚══════╝  ░╚════╝░░░░╚═╝░░░╚══════╝╚═╝░░╚═╝');
  }

  String toString() {
    return 'Robot $fuel';
  }
  

}