import 'Management.dart';

class CheckWin extends Management {
  CheckWin(num x, num y, String symbol) : super(x, y, symbol);

  @override
  String getSymbol() {
    symbol = '  ✪  ';
    return symbol;
  }

  @override
  num getX() {
    return x;
  }

  @override
  num getY() {
    return x;
  }

  @override
  bool isOn(num x, num y) {
    return this.x == x && this.y == y;
  }
}